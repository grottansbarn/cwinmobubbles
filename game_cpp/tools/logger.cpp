// logger.cpp - abstraction

#include "logger.hpp"
#include "logger_impl.hpp"

logger::logger(logger_impl* p)
    : pimpl(p)
{
}

logger::~logger()
{
    delete pimpl;
}

console_logger::console_logger()
    : logger(
#ifdef MT
          new MT_loggerImpl()
#else
          new ST_logger_impl()
#endif
              )
{
}

void console_logger::log(const std::string& str)
{
    pimpl->console_log(str);
}

file_logger::file_logger(const std::string& file_name)
    : logger(
#ifdef MT
          new MT_loggerImpl()
#else
          new ST_logger_impl()
#endif
              )
    , file(file_name)
{
}

void file_logger::log(const std::string& str)
{
    pimpl->file_log(file, str);
}

socket_logger::socket_logger(std::string& remote_host,
    int remote_port)
    : logger(
#ifdef MT
          new MT_loggerImpl()
#else
          new ST_logger_impl()
#endif
              )
    , host(remote_host)
    , port(remote_port)
{
}

void socket_logger::log(const std::string& str)
{
    pimpl->socket_log(host, port, str);
}
