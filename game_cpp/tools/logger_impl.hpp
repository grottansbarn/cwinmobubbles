// LoggerImpl.h - implementation
#pragma once

#include <string>

class logger_impl {
public:
    virtual void console_log(const std::string& str) = 0;
    virtual void file_log(std::string& file, const std::string& str) = 0;
    virtual void socket_log(std::string& host, int port, const std::string& str) = 0;
    virtual ~logger_impl() {}
};

class ST_logger_impl : public logger_impl {
public:
    void console_log(const std::string& str);
    void file_log(std::string& file, const std::string& str);
    void socket_log(std::string& host, int port, const std::string& str);
};

class MT_logger_impl : public logger_impl {
public:
    void console_log(const std::string& str);
    void file_log(std::string& file, const std::string& str);
    void socket_log(std::string& host, int port, const std::string& str);
};
