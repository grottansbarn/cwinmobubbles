#include <memory>

#include "game.hpp"
#include "state_select_mode.hpp"

int main(int /*argc*/, char** /*argv*/)
{
    WINMO_RESULT result = WINMO_OK;

    std::unique_ptr<game> g = std::make_unique<game>();


    if(WINMO_FAILED(result = g->initialize()))
        return result;

    if(WINMO_FAILED(result = g->run()))
        return result;

    return EXIT_SUCCESS;
}
