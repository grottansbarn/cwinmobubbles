cmake_minimum_required(VERSION 3.16)

project(game)

file(GLOB_RECURSE GAME_SRC_FILES
    "${CMAKE_CURRENT_LIST_DIR}/*.cpp"
    )

file(GLOB_RECURSE GAME_HDR_FILES
    "${CMAKE_CURRENT_LIST_DIR}/*.hpp"
    )

if(ANDROID)
    add_library(${PROJECT_NAME} SHARED ${GAME_SRC_FILES} ${GAME_HDR_FILES})
    set_target_properties(${PROJECT_NAME} PROPERTIES ENABLE_EXPORTS TRUE)
else()
    add_executable(${PROJECT_NAME} ${GAME_SRC_FILES} ${GAME_HDR_FILES})
endif()

target_include_directories(${PROJECT_NAME}
    PUBLIC
        "${CMAKE_CURRENT_LIST_DIR}"
        "${CMAKE_CURRENT_LIST_DIR}/states"
        "${CMAKE_CURRENT_LIST_DIR}/tools"
        "${CMAKE_CURRENT_LIST_DIR}/components"
)

target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)

target_link_libraries(${PROJECT_NAME} PRIVATE engine)

set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME ${EXECUTABLE_NAME})
