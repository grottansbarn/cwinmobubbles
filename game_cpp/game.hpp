#pragma once

#include <thread>
#include <vector>

#include "engine.h"


using clock_timer = std::chrono::high_resolution_clock;
using nano_sec = std::chrono::nanoseconds;
using milli_sec = std::chrono::milliseconds;
using time_point = std::chrono::time_point<clock_timer, nano_sec>;

#define FRAME_TIME 15

class game;

class game_state {
public:

    game_state()
        : pGame{ WINMO_NULLPTR }
        , pEngine{ WINMO_NULLPTR }
    {
    }

    WINMO_RESULT set_game(game* pG);

    virtual WINMO_RESULT init(WINMO_ENGINE_PTR pE) = 0;
    virtual WINMO_RESULT cleanup() = 0;
    virtual WINMO_RESULT destroy() = 0;

    virtual WINMO_RESULT pause() = 0;
    virtual WINMO_RESULT resume() = 0;

    virtual WINMO_RESULT handle_events() = 0;
    virtual WINMO_RESULT update() = 0;
    virtual WINMO_RESULT draw() = 0;

    virtual ~game_state();


    game* pGame;

protected:
    WINMO_ENGINE_PTR pEngine;
};


class game final {
public:
    game();

    // game(game_state* state);

    ~game();

    WINMO_RESULT initialize();

    WINMO_RESULT run();

    WINMO_RESULT transition_to_state(game_state* new_state);

    game(game const&) = delete;
    game& operator=(game const&) = delete;
    game(game&&) = delete;
    game& operator=(game&&) = delete;


    game_state* select_mode_state;
    game_state* game_over_state;

private:
    clock_timer timer;
    time_point start;
    time_point end_last_frame;
    milli_sec g_frame_delta;

    WINMO_ENGINE_PTR pEngine;

    game_state* current_state;
};
