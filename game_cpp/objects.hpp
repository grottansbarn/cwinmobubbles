#pragma once

#include "engine.h"

namespace winmo {


class Object {
public:
    virtual WINMO_RESULT draw() = 0;

    virtual WINMO_BOOL   isClicked() = 0;
    virtual WINMO_BOOL   isUnderCursor() = 0;

    // virtual WINMO_RESULT move() = 0;

    Object(WINMO_ENGINE_PTR pEngine) //, SDL_Rect rectangle)
        : pE {pEngine}
    {
        // rect.x = rectangle.x;
        // rect.y = rectangle.y;
        // rect.w = rectangle.w;
        // rect.h = rectangle.h;
    }

    ~Object() = default;
    Object() = delete;
    Object(const Object&) = delete;
    const Object& operator=(const Object&) = delete;
    Object(Object&&) = delete;
    Object& operator=(Object&&) = delete;

public:
    WINMO_ENGINE_PTR pE; 
    // SDL_Rect     rect;
};


} // end of the namespace