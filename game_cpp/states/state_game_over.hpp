#pragma once

#include <memory>

#include "engine.h"

#include "game.hpp"

namespace winmo {

class state_game_over : public game_state {
public:
    state_game_over()
        : pFont { WINMO_NULL }
        , pTextureBack { WINMO_NULL }
        , pTextureGoodBye { WINMO_NULL }
        , pRectGoodBye { WINMO_NULL }
    {}

    WINMO_RESULT init(WINMO_ENGINE_PTR pE) override;
    WINMO_RESULT cleanup() override;
    WINMO_RESULT destroy() override;

    WINMO_RESULT pause() override;
    WINMO_RESULT resume() override;

    WINMO_RESULT handle_events() override;
    WINMO_RESULT update() override;
    WINMO_RESULT draw() override;

    // WINMO_RESULT handle_mouse_event();

    WINMO_RESULT sound_turn_on();
    WINMO_RESULT sound_turn_off();


    ~state_game_over() override;

    state_game_over(state_game_over const&) = delete;
    state_game_over& operator=(state_game_over const&) = delete;

    static const WINMO_INT move_offset = 50;

private:

    TTF_Font*    pFont;
    SDL_Texture* pTextureBack;
    SDL_Texture* pTextureGoodBye;
    SDL_Rect*    pRectGoodBye;
};

} // end of namespace
