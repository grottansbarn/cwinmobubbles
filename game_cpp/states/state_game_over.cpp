#include <fstream>

#include "state_game_over.hpp"
#include "state_select_mode.hpp"

namespace winmo {

state_game_over::~state_game_over()
{
    engine_texture_destroy(pTextureBack);
    engine_texture_destroy(pTextureGoodBye);
    engine_font_close(pFont);
}

WINMO_RESULT state_game_over::init(WINMO_ENGINE_PTR pE)
{
    WINMO_RESULT result = WINMO_OK;

    if(!pE) return WINMO_E_INVARGS;

    pEngine = pE;

    pTextureBack = engine_create_texture_from_png(pEngine, "./data/images/gui/back_1.png");
    if (! pTextureBack){
        result = WINMO_E_SDL;
    }

    pFont = engine_font_open("./data/font/consola.ttf", 64);
    if(! pFont) {
        result =  WINMO_E_FONT_OPEN;
        return result;
    }

    SDL_Color color = {0, 0, 255, 0};
    SDL_Color bg_color = {100, 100, 100, 122};
    pTextureGoodBye = engine_create_texture_from_text("good bye!", pFont, color, bg_color, pEngine->pRenderer);
    if(pTextureGoodBye == WINMO_NULL) {
        result = WINMO_E_TEX_CREATE_FAIL;
        return result;
    }

    pRectGoodBye = WINMO_MALLOC(SDL_Rect, 1);
    if(WINMO_NULL == pRectGoodBye)
        return WIMNO_E_OUTOFMEMORY;
    pRectGoodBye->x = pRectGoodBye->y = 100;
    pRectGoodBye->h = pRectGoodBye->w = 200;

    return result;
}

WINMO_RESULT state_game_over::cleanup()
{
    return WINMO_OK;
}

WINMO_RESULT state_game_over::destroy()
{
    return WINMO_OK;
}

WINMO_RESULT state_game_over::pause()
{
    return WINMO_OK;
}

WINMO_RESULT state_game_over::resume()
{
    return WINMO_OK;
}

WINMO_RESULT state_game_over::sound_turn_on()
{
    return WINMO_OK;
}

WINMO_RESULT state_game_over::sound_turn_off()
{
    return WINMO_OK;
}

// WINMO_RESULT state_game_over::handle_mouse_event()
// {
//     return WINMO_OK;
// }

WINMO_RESULT state_game_over::handle_events()
{
    WINMO_RESULT result = WINMO_OK;

    if (pEngine->event == event_turn_off) {
        pEngine->loop = WINMO_FALSE;
    }

    if (pEngine->event == event_right && pEngine->event_type == eventtype_released) {
        pRectGoodBye->x += move_offset;
    }
    if (pEngine->event == event_left && pEngine->event_type == eventtype_released) {
        pRectGoodBye->x -= move_offset;
    }
    if (pEngine->event == event_up && pEngine->event_type == eventtype_released) {
        pRectGoodBye->y -= move_offset;
    }
    if (pEngine->event == event_down && pEngine->event_type == eventtype_released) {
        pRectGoodBye->y += move_offset;
    }

    if (pEngine->event == event_escape && pEngine->event_type == eventtype_released) {
        pGame->transition_to_state(pGame->select_mode_state);
    }

    return WINMO_OK;
}

WINMO_RESULT state_game_over::update()
{
    return WINMO_OK;
}

WINMO_RESULT state_game_over::draw()
{
    WINMO_RESULT result = WINMO_OK;

    if(WINMO_FAILED(result = engine_render_clear(pEngine, pEngine->pRenderer)))
        return result;

    if(WINMO_FAILED(result = engine_render_copy(pEngine, pEngine->pRenderer, pTextureBack, NULL, NULL)))
        return result;

    if(WINMO_FAILED(result = engine_render_copy(pEngine, pEngine->pRenderer, pTextureGoodBye, NULL, pRectGoodBye)))
        return result;

    if(WINMO_FAILED(result = engine_render_present(pEngine, pEngine->pRenderer)))
        return result;

    return result;
}

} // end of namespace