#pragma once

#include <memory>

#include "engine.h"

#include "game.hpp"

namespace winmo {

class state_select_mode : public game_state {
public:
    state_select_mode()
        : pTextureBack { WINMO_NULL }
        , pClickSoundBuf { WINMO_NULL }
    {}

    WINMO_RESULT init(WINMO_ENGINE_PTR pE) override;
    WINMO_RESULT cleanup() override;
    WINMO_RESULT destroy() override;

    WINMO_RESULT pause() override;
    WINMO_RESULT resume() override;

    WINMO_RESULT handle_events() override;
    WINMO_RESULT update() override;
    WINMO_RESULT draw() override;

    // WINMO_RESULT handle_mouse_event();

    WINMO_RESULT sound_turn_on();
    WINMO_RESULT sound_turn_off();

    ~state_select_mode() override;

    state_select_mode(state_select_mode const&) = delete;
    state_select_mode& operator=(state_select_mode const&) = delete;

private:

    SDL_Texture* pTextureBack;
    SOUNDBUFFER_PTR pClickSoundBuf;
};

} // end of namespace
