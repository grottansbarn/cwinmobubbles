#include <iostream>

#include "state_select_mode.hpp"
#include "state_game_over.hpp"

namespace winmo {

state_select_mode::~state_select_mode()
{
    engine_texture_destroy(pTextureBack);
    engine_soundbuffer_destroy(pClickSoundBuf);
}

WINMO_RESULT state_select_mode::init(WINMO_ENGINE_PTR pE)
{
    WINMO_RESULT result = WINMO_OK;

    if(!pE) return WINMO_E_INVARGS;

    pEngine = pE;

    pTextureBack = engine_create_texture_from_png(pEngine, "./data/images/gui/back_2.png");
    if (! pTextureBack){
        result = WINMO_E_SDL;
    }

    if(WINMO_FAILED(result = engine_soundbuffer_create(pEngine, &pClickSoundBuf, "./data/sounds/10_sound_on.wav")))
        return result;

    return result;
}

WINMO_RESULT state_select_mode::cleanup()
{
    return WINMO_OK;
}

WINMO_RESULT state_select_mode::destroy()
{
    return WINMO_OK;
}

WINMO_RESULT state_select_mode::pause()
{
    return WINMO_OK;
}

WINMO_RESULT state_select_mode::resume()
{
    return WINMO_OK;
}

WINMO_RESULT state_select_mode::sound_turn_on()
{
    return WINMO_OK;
}

WINMO_RESULT state_select_mode::sound_turn_off()
{
    return WINMO_OK;
}

// WINMO_RESULT state_select_mode::handle_mouse_event()
// {
//     return WINMO_OK;
// }

WINMO_RESULT state_select_mode::handle_events()
{
    WINMO_RESULT result = WINMO_OK;

    if (pEngine->event == event_turn_off){
        pEngine->loop = WINMO_FALSE;
    }

    if(pEngine->event == event_mouse && pEngine->event_type == eventtype_pressed)
    {
        if(WINMO_FAILED(result = engine_soundbuffer_play(pEngine, pClickSoundBuf, sound_play_once))) {
            std::cerr << "Can't play sound: " << pClickSoundBuf->path_to_src_file << std::endl;
            return result;
        }

        pGame->transition_to_state(pGame->game_over_state);
    }

    return WINMO_OK;
}

WINMO_RESULT state_select_mode::update()
{
    return WINMO_OK;
}

WINMO_RESULT state_select_mode::draw()
{
    WINMO_RESULT result = WINMO_OK;

    if(WINMO_FAILED(result = engine_render_clear(pEngine, pEngine->pRenderer)))
        return result;

    if(WINMO_FAILED(result = engine_render_copy(pEngine, pEngine->pRenderer, pTextureBack, NULL, NULL)))
        return result;

    if(WINMO_FAILED(result = engine_render_present(pEngine, pEngine->pRenderer)))
        return result;

    return result;
}

} // end of namespace