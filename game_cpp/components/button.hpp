#pragma once

#include <vector>

#include "engine.h"
// #include "objects.hpp"

namespace winmo {

#define MAX_BUTTONS   8

/**
 *  \brief Button class containing id, rect, text, etc.
 */
class Button {
public:
    Button()
        : id {0}
        , text {nullptr}
        , font {nullptr}
        , font_color {0}
        , texture {nullptr}
        , rect {0}
        , bg_color {0, 0, 0, 0}
    {}

    WINMO_INT    id;
    WINMO_PCHAR  text;
    TTF_Font*    font;
    SDL_Color    font_color;
    SDL_Texture* texture;
    SDL_Rect     rect;
    SDL_Color    bg_color;
};


/**
 *  \brief Buttons class represent all buttons used in current state
 */
class Buttons {
public:

    Buttons()
        : id {0}
        , pressed_index {0}
        , counter {0}
        , buttons {nullptr}
        , pEngine {nullptr}
    {}

    Buttons(const WINMO_ENGINE_PTR pEngine);

    ~Buttons();

    /**
     *  \brief Add new button to buttons set
     *
     *  \param ppButtons pointer to buttons set
     *  \param text of the button, width is set automatically
     *  \param text_size in pixels
     *  \param x coordinate of the left hi corner
     *  \param y coordinate of the left hi conrner
     *
     *  \return WINMO_OK or error code on error
     */
    WINMO_RESULT add_button(WINMO_PCHAR text, const WINMO_INT& text_size, const WINMO_INT& x, const WINMO_INT& y);

    /**
     *  \brief returns WINMO_ERROR if there aren't any button in this coordinates
     *          in othes cases changes the 'pressed_index'
     */
    WINMO_RESULT get_pressed_index(const WINMO_INT& x, const WINMO_INT& y);

    /**
     *  \brief render all buttons from set
     * 
     *  \return WINMO_OK or error code on error
     */
    WINMO_RESULT buttons_render();

    WINMO_INT   id;
    WINMO_INT   pressed_index;
    WINMO_INT   counter;

    std::vector<Button*> buttons;

    WINMO_ENGINE_PTR pEngine;
};

} // end of the namespace