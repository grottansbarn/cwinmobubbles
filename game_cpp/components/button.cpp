#include <stdio.h>

#include "button.hpp"

namespace winmo {

// //globals vars
// extern WINMO_ENGINE_PTR pEngine;

Buttons::Buttons(const WINMO_ENGINE_PTR pEngine)
{
    WINMO_RESULT result = WINMO_OK;

    id = 0;
    pressed_index = -1;
    counter = 0;

    printf("Buttons set created\n");
}


WINMO_RESULT
Buttons::add_button(WINMO_PCHAR text, const WINMO_INT& text_size, const WINMO_INT& x, const WINMO_INT& y)
{
    WINMO_RESULT result = WINMO_OK;
    Button* pB = WINMO_NULL;
    WINMO_INT w = 0;
    WINMO_INT h = 0;
    SDL_Color font_color = {0, 255, 0, 0};
    SDL_Color font_bg_color = {0, 0, 0, 0};

    if(!text)
        return WINMO_E_INVARGS;

    pB = new Button();
    if(!pB)
        return WIMNO_E_OUTOFMEMORY;

    pB->id = counter;

    pB->font = engine_font_open("./data/font/consola.ttf", text_size);
    if(!pB->font)
    {
        result = WINMO_E_FONT_OPEN;
        goto exit_error_1;
    }

    pB->texture = engine_create_texture_from_text(text, pB->font, font_color, font_bg_color, (SDL_Renderer*)pEngine->pRenderer);
    if(!pB->texture)
    {
        result = WINMO_E_FONT_TEXT_CREATE;
        goto exit_error_2;
    }

    if(WINMO_FAILED(result = engine_get_texture_size(pB->texture, &w, &h)))
        goto exit_error_3;

    printf("Got texture size: w= %d, h= %d \n", w, h);

    pB->rect.x = x; pB->rect.y = y; pB->rect.w = w; pB->rect.h = h;
    pB->text = text;

    // insert address with vacant id in array
    buttons[counter] = pB;

    printf("Button %d '%s' added \n", counter, text);

    counter++;

    return WINMO_OK;

exit_error_3:
    engine_texture_destroy(pB->texture);

exit_error_2:
    TTF_CloseFont(pB->font);

exit_error_1:
    free(pB);

    return result;
}


WINMO_RESULT
Buttons::get_pressed_index(const WINMO_INT& x, const WINMO_INT& y)
{
    if(x > pEngine->screen_width || x < 0 || y > pEngine->screen_height || y < 0)
        return WINMO_E_INVARGS;

    for (WINMO_INT i = 0; i < counter; i++)
    {
        if (( x >= buttons[i]->rect.x ) &&
            ( x <= ( buttons[i]->rect.x + buttons[i]->rect.w )) &&
            ( y >= buttons[i]->rect.y ) &&
            ( y <= ( buttons[i]->rect.y + buttons[i]->rect.h )))
        {
            pressed_index = i;
            return WINMO_OK;
        }
    }

    pressed_index = -1;

    return WINMO_OK;
}


WINMO_RESULT
Buttons::buttons_render()
{
    WINMO_RESULT result = WINMO_OK;

    for(int i = 0; i < counter; i++)
    {
        result = engine_render_copy(pEngine, pEngine->pRenderer, buttons[i]->texture, NULL, &(buttons[i]->rect));
        if(-1 == result){
            fprintf(stderr, "Can't render button %d '%s'", i, buttons[i]->text);
            result = WINMO_E_BUTTON_RENDER;
            break;
        }
    }

    return result;
}


Buttons::~Buttons()
{
    for(WINMO_INT i = 0; i < counter; i++)
    {
        if(buttons[i] != WINMO_NULL) {
            printf("Removed button %d '%s' \n", i, buttons[i]->text);
            delete buttons[i];
        }
    }
}

} // end of the namespace