#include <iostream>
#include <typeinfo>

#include "game.hpp"

#include "state_select_mode.hpp"
#include "state_game_over.hpp"

/*****************************************************************************/
WINMO_RESULT
game_state::set_game(game* pG)
{
    pGame = pG;
    return WINMO_OK;
}

game_state::~game_state(){}

/*****************************************************************************/
game::game()
{
    pEngine = nullptr;
    select_mode_state = nullptr;
    game_over_state = nullptr;
    current_state = nullptr;

    printf("Game default constructor\n");
}


// game::game(game_state* state) 
// {
//     std::cout << "Game constructor with state" << typeid(*state).name() << std::endl;

//     current_state = state;

//     transition_to_state(current_state);
// }


WINMO_RESULT 
game::initialize()
{
    WINMO_RESULT result = WINMO_OK;

    if(WINMO_FAILED(result = engine_create(&pEngine)))
    {
        fprintf(stderr, "Error (%X): cannot create Engine\n", result);
        return result;
    }

    if(WINMO_FAILED(result = engine_sound_init(pEngine)))
    {
        fprintf(stderr, "Error (%X): cannot initialize sound\n", result);
        return result;
    }

    select_mode_state = reinterpret_cast<game_state*>(new winmo::state_select_mode());
    if(WINMO_FAILED(result = select_mode_state->init(pEngine)))
    {
        fprintf(stderr, "Error (%X): cannot initialize state_select_mode\n", result);
        return result;
    }
    select_mode_state->pGame = this;

    game_over_state = reinterpret_cast<game_state*>(new winmo::state_game_over());
    if(WINMO_FAILED(result = game_over_state->init(pEngine)))
    {
        fprintf(stderr, "Error (%X): cannot initialize state_select_mode\n", result);
        return result;
    }
    game_over_state->pGame = this;

    current_state = select_mode_state;

    return result;
}


WINMO_RESULT 
game::run()
{
    WINMO_RESULT result = WINMO_OK;
    //main loop
    start = timer.now();

    while (pEngine->loop) {
        end_last_frame = timer.now();

        if(WINMO_FAILED(result = engine_update_event(pEngine)))
            return result;

        current_state->handle_events();
        current_state->update();
        current_state->draw();

        g_frame_delta = std::chrono::duration_cast<milli_sec>(end_last_frame - start);
        if (g_frame_delta.count() < FRAME_TIME) { //~60fps
            std::this_thread::yield();
        }
        start = end_last_frame;
    }

    return result;
}


WINMO_RESULT   // change to winmo_bubbles approach with initializing only one object of the state
game::transition_to_state(game_state* new_state) 
{
    WINMO_RESULT result = WINMO_OK;

    std::cout << "Game: Transition to " << typeid(*new_state).name() << ".\n";

    current_state->pause();

    current_state = new_state;

    current_state->resume();

    return result;
}


game::~game()
{
    select_mode_state->destroy();
    delete select_mode_state;

    game_over_state->destroy();
    delete game_over_state;


    current_state = nullptr;

    engine_destroy(pEngine);
    pEngine = nullptr;

    printf("Game destructor\n");
}
