#pragma once

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include "engine.h"

using namespace testing;


TEST(sound_tests, soundmanager_buffer_create_inval_args_1)
{
    WINMO_RESULT expected = WINMO_E_INVARGS;
    WINMO_ENGINE_PTR pEngine = WINMO_NULL; // INVALID
    SOUNDBUFFER_PTR pSoundBuf = WINMO_NULL;
    const char* pPath = "../data/sounds/10_sound_on.wav";
    WINMO_RESULT test = engine_soundbuffer_create(pEngine, &pSoundBuf, pPath);
    EXPECT_EQ(expected, test);
}

TEST(sound_tests, soundmanager_buffer_create_inval_args_2)
{
    WINMO_RESULT expected = WINMO_E_INVARGS;
    WINMO_ENGINE_PTR pEngine = WINMO_MALLOC(WINMO_Engine, 1);
    SOUNDBUFFER_PTR pSoundBuf = WINMO_MALLOC(SoundBuffer, 1); // INVALID
    const char* pPath = "../data/sounds/10_sound_on.wav";
    WINMO_RESULT test = engine_soundbuffer_create(pEngine, &pSoundBuf, pPath);
    EXPECT_EQ(expected, test);
}

TEST(sound_tests, soundmanager_buffer_create_inval_args_3)
{
    WINMO_RESULT expected = WINMO_E_INVARGS;
    WINMO_ENGINE_PTR pEngine = WINMO_MALLOC(WINMO_Engine, 1);
    SOUNDBUFFER_PTR pSoundBuf = WINMO_NULL;
    const char* pPath = WINMO_NULL; // INVALID
    WINMO_RESULT test = engine_soundbuffer_create(pEngine, &pSoundBuf, pPath);
    EXPECT_EQ(expected, test);
}

TEST(sound_tests, soundmanager_buffer_create_inval_path)
{
    WINMO_RESULT expected = WINMO_E_FILE;
    WINMO_ENGINE_PTR pEngine = WINMO_MALLOC(WINMO_Engine, 1);
    SOUNDBUFFER_PTR pSoundBuf = WINMO_NULL;
    const char* pPath = "unexisted_file.wav";
    WINMO_RESULT test = engine_soundbuffer_create(pEngine, &pSoundBuf, pPath);
    EXPECT_EQ(expected, test);
}

TEST(sound_tests, soundmanager_buffer_destroy_inval_arg)
{
    WINMO_RESULT expected = WINMO_E_INVARGS;
    SOUNDBUFFER_PTR pSoundBuf = WINMO_NULL;
    WINMO_RESULT test = engine_soundbuffer_destroy(pSoundBuf);
    EXPECT_EQ(expected, test);
}

TEST(sound_tests, soundmanager_buffer_destroy_valid_arg)
{
    WINMO_RESULT expected = WINMO_OK;
    const char* pPath = "../data/sounds/10_sound_on.wav";
    SOUNDBUFFER_PTR pSoundBuf = WINMO_MALLOC(SoundBuffer, 1);
    WINMO_SIZE tmp = strlen(pPath);
    pSoundBuf->path_to_src_file = (const char*)malloc(tmp + 1);
    strncpy((char*)pSoundBuf->path_to_src_file, pPath, tmp + 1);
    // pSoundBuf->buffer = malloc(128);
    WINMO_RESULT test = engine_soundbuffer_destroy(pSoundBuf);
    EXPECT_EQ(expected, test);
}