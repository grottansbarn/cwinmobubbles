#include "linked_list.h"

extern int Error;

LINK_LIST_NODE_PTR
create_list(WINMO_INT number)
{
    LINK_LIST_NODE_PTR p_begin = NULL;
    LINK_LIST_NODE_PTR p = NULL;

    p_begin = WINMO_MALLOC(LinkListNode, 1);
    if(!p_begin){
        Error = WIMNO_E_OUTOFMEMORY;
        return p_begin;
    }
    p = p_begin;
    p->next = NULL;
    p->value = 0;
    for(int k = 1; k < number; k++){
        p->next = WINMO_MALLOC(LinkListNode, 1);
        if(!p->next){
            Error = WIMNO_E_OUTOFMEMORY;
            return p_begin;
        }
        // step forward
        p = p->next;
        // fill new object
        p->next = NULL;
        p->value = k;
    }
    return p_begin;
}

void
print_list(LINK_LIST_NODE_PTR pList)
{
    LINK_LIST_NODE_PTR p = pList;
    while (p != NULL){
        printf("%d \t", p->value);
        // mode to forward node
        p = p->next;
    }
}

void
delete_list(LINK_LIST_NODE_PTR pList)
{
    LINK_LIST_NODE_PTR p = pList;
    while (p != NULL){
        LINK_LIST_NODE_PTR tmp;
        tmp = p;
        // move forward
        p = p->next;
        // delete current node
        free(tmp);
    }
}

