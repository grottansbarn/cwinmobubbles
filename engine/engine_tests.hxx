#pragma once

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include "types.h"

using namespace testing;

TEST(types_tests, winmo_true_1)
{
    EXPECT_EQ(WINMO_TRUE, 1);
}
