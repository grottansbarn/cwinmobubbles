#include "engine.h"


// struct WINMO_Renderer {
//     SDL_Renderer* pRenderer;
// };


WINMO_RESULT
engine_render_clear(WINMO_ENGINE_PTR pEngine, SDL_Renderer* pRenderer)
{
    WINMO_PCHAR err_message = WINMO_NULL;

    if(!pEngine || !pRenderer)
        return WINMO_E_INVARGS;

    if(SDL_FAILED(SDL_RenderClear(pRenderer))) {
        err_message = SDL_GetError();
        fprintf(stderr, "Can't clear render (%X): %s \n", WINMO_E_SDL, err_message);
        return WINMO_E_SDL;
    }

    return WINMO_OK;
}


WINMO_RESULT
engine_render_copy(WINMO_ENGINE_PTR pEngine, SDL_Renderer* pRenderer, SDL_Texture* pTexture,
    const SDL_Rect* pSrcRect /* can be WINMO_NULL*/,
    const SDL_Rect* pDstRect /* can be WINMO_NULL*/)
{
    WINMO_PCHAR err_message = WINMO_NULL;

    if(!pEngine || !pRenderer)
        return WINMO_E_INVARGS;

    if(SDL_FAILED(SDL_RenderCopy(pRenderer, pTexture, pSrcRect, pDstRect))) {
        err_message = SDL_GetError();
        fprintf(stderr, "Can't render copy (%X): %s \n", WINMO_E_SDL, err_message);
        return WINMO_E_SDL;
    }
    
    return WINMO_OK;
}


WINMO_RESULT
engine_render_present(WINMO_ENGINE_PTR pEngine, SDL_Renderer* pRenderer)
{
    if(!pEngine || !pRenderer)
        return WINMO_E_INVARGS;

    SDL_RenderPresent(pRenderer);

    return WINMO_OK;
}
