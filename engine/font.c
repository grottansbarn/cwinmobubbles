#include "engine.h"

TTF_Font*
engine_font_open(const char* pFontPath, const WINMO_INT size)
{
    const char* err_message = WINMO_NULL;

    if (!pFontPath) {
        fprintf(stderr, "Can't open file: %s \n", pFontPath);
        return WINMO_NULL;
    }

    TTF_Font* pFont = TTF_OpenFont(pFontPath, size);

    if (pFont == WINMO_NULL) {
        err_message = SDL_GetError();
        fprintf(stderr, "Can't open font - %s: %s \n", pFontPath, err_message);
        return WINMO_NULL;
    }

    return pFont;
}


SDL_Texture*
engine_create_texture_from_text(
    const char*     pText,
    TTF_Font*       pFont, 
    const SDL_Color text_color,
    const SDL_Color text_bg_color,
    SDL_Renderer*   pRenderer)
{
    SDL_Surface* pSurf = WINMO_NULL;
    SDL_Texture* pTexture = WINMO_NULL;
    const char* err_message = WINMO_NULL;
    
    // pSurf = TTF_RenderText_Blended(font, text, text_color); // without bg
    pSurf = TTF_RenderText_Shaded(pFont, pText, text_color, text_bg_color); // with bg
    if (pSurf == WINMO_NULL) {
        err_message = SDL_GetError();
        fprintf(stderr, "Can't blend text to surface - %s: %s \n", pText, err_message);
        return WINMO_NULL;
    }

    pTexture = SDL_CreateTextureFromSurface(pRenderer, pSurf);
    if (pTexture == WINMO_NULL) {
        err_message = SDL_GetError();
        fprintf(stderr, "Can't create texture from text - %s: %s \n", pText, err_message);
        return WINMO_NULL;
    } else {
        SDL_FreeSurface(pSurf);
        printf("Created texture from text: '%s' \n", pText);
    }

    return pTexture;
}


WINMO_RESULT
engine_font_close(TTF_Font* pFont)
{
    if(!pFont)
        return WINMO_OK;

    TTF_CloseFont(pFont);

    return WINMO_OK;
}
