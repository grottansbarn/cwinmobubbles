#ifndef MACRO_H
#define MACRO_H

#ifdef __cplusplus
extern "C" {
#endif

#define IN
#define OUT

#define WINMO_OK                    0x0000

#define WINMO_ERROR                 0x8000
#define WINMO_E_INVARGS             0x8001
#define WINMO_E_DOUBLEFREE          0x8002
#define WIMNO_E_OUTOFMEMORY         0x8003
#define WINMO_E_FILE                0x8004
#define WINMO_E_TEX_CREATE_FAIL     0x8005
#define WINMO_E_RENDER_CREATE_FAIL  0x8006
#define WINMO_E_UNKNOWN_KEYCODE     0x8007
#define WINMO_E_FONT_OPEN           0x8030
#define WINMO_E_FONT_TEXT_CREATE    0x8031
#define WINMO_E_FONT_GET_TEX_SIZE   0x8032
#define WINMO_E_BUTTON_RENDER       0x8040
#define WINMO_E_SOUND_QUEUE         0x8050
#define WINMO_E_SDL                 0x8300

#define WINMO_SUCCESS(x)            (((x) & 0x8000) == 0)
#define WINMO_FAILED(x)             (((x) & 0x8000) != 0)
#define SDL_FAILED(x)               ((x) == -1)

#define WINMO_MALLOC(type, count)   (type*)malloc(sizeof(type)*(count))
#define WINMO_FREE(data)            free(data)


#ifdef __cplusplus
}
#endif

#endif
