#ifndef ENGINE_H
#define ENGINE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"

/* common engine headers */
#include "macro.h"
#include "types.h"


/* mouse *********************************************************************/

typedef struct tagMousePosition {
    WINMO_INT x;
    WINMO_INT y;
} MousePosition, *MOUSE_POSITION;


/* texture *******************************************************************/

struct tagWinMoTexture;
typedef struct tagWinMoTexture WinMoTexture;
typedef struct tagWinMoTexture* WINMO_TEXTURE_PTR;


/* render ********************************************************************/

struct tagWinMoRenderer;
typedef struct tagWinMoRenderer WinMoRenderer;
typedef struct tagWinMoRenderer* WINMO_RENDERER_PTR;


/* events ********************************************************************/

typedef enum tagEvent {
    event_left,
    event_right,
    event_up,
    event_down,
    event_escape,
    event_enter,
    event_mouse_motion, // on mouse move
    event_mouse,
    event_turn_off,
    event_non
} Event, *EVENT_PTR;

typedef enum tagEventType {
    eventtype_pressed,
    eventtype_released,
    eventtype_non
} EventType, *EVENT_TYPE_PTR;


/* sound *********************************************************************/

#define MAX_TEMP_BUF_DEFAULT  256
#define MAX_SOUNDS_DEFAULT    16

struct tagWinMoAudioDevice;
typedef struct tagWinMoAudioDevice WinMoAudioDevice;
typedef struct tagWinMoAudioDevice* WINMO_AUDIO_DEVICE_PTR;

typedef enum tagSoundPlayProp {
    sound_play_once,
    sound_play_looped
} SoundPlayProp;

typedef struct tagSoundBuffer {
    uint8_t*            buffer;
    uint32_t            length;
    WINMO_BOOL          is_playing; // = false;
    WINMO_BOOL          is_looped; // = false;
    uint8_t*            tmp_buf;
    WINMO_PCHAR         path_to_src_file;
} SoundBuffer, *SOUNDBUFFER_PTR;


/* engine ********************************************************************/

typedef struct tagEngine {
    WINMO_INT     screen_height;
    WINMO_INT     screen_width;

    Event         event;
    EventType     event_type; // pressed or released flag
    MousePosition mouse_coord;
    MousePosition mouse_coord_pressed;
    MousePosition mouse_coord_released;

    SDL_AudioDeviceID  audio_device;
    SDL_AudioSpec      audio_device_spec;

    SDL_Window*   pWindow;
    SDL_Renderer* pRenderer;

    WINMO_BOOL    loop;
} WINMO_Engine, *WINMO_ENGINE_PTR;


WINMO_RESULT
engine_sound_init(IN OUT WINMO_ENGINE_PTR pEngine);


WINMO_RESULT
engine_create(IN OUT WINMO_ENGINE_PTR* pEngine);


WINMO_RESULT
engine_destroy(IN WINMO_ENGINE_PTR pEngine);


/* auxiliary functions *******************************************************/

WINMO_PCHAR
engine_get_sound_format_name(uint16_t format_value);


WINMO_INT
engine_get_sound_format_size(uint16_t format_value);


/* events ********************************************************************/

WINMO_RESULT
engine_update_event(IN WINMO_ENGINE_PTR pEngine);


/* fonts *********************************************************************/

TTF_Font*
engine_font_open(WINMO_PCHAR pFontPath, const WINMO_INT size);


SDL_Texture*
engine_create_texture_from_text(WINMO_PCHAR pMessage, TTF_Font* pFont, 
    const SDL_Color text_color, const SDL_Color text_bg_color, SDL_Renderer* pRenderer);


WINMO_RESULT
engine_font_close(TTF_Font* pFont);


/* sound *********************************************************************/

WINMO_RESULT
engine_soundbuffer_create(IN WINMO_ENGINE_PTR pEngine, IN OUT SOUNDBUFFER_PTR* ppSoundBuf, IN WINMO_PCHAR path);


WINMO_RESULT
engine_soundbuffer_play(IN WINMO_ENGINE_PTR pEngine, IN SOUNDBUFFER_PTR pSoundBuf, IN SoundPlayProp prop);


WINMO_RESULT
engine_soundbuffer_destroy(IN SOUNDBUFFER_PTR pSoundBuf);


/* texture *******************************************************************/

SDL_Texture*
engine_create_texture_from_bmp(IN WINMO_ENGINE_PTR pEngine, WINMO_PCHAR pBmpFile);


SDL_Texture*
engine_create_texture_from_png(IN WINMO_ENGINE_PTR pEngine, WINMO_PCHAR pPngFile);


WINMO_RESULT
engine_texture_destroy(SDL_Texture* pTexture);


WINMO_RESULT
engine_get_texture_size(SDL_Texture* pTexture, WINMO_INT* pWidth, WINMO_INT* pHeight);


/* render ********************************************************************/

WINMO_RESULT
engine_render_clear(WINMO_ENGINE_PTR pEngine, SDL_Renderer* pRenderer);


WINMO_RESULT
engine_render_copy(WINMO_ENGINE_PTR pEngine, SDL_Renderer* pRenderer, SDL_Texture* pTexture,
    const SDL_Rect* pSrcRect, const SDL_Rect* pDstRect);


WINMO_RESULT
engine_render_present(WINMO_ENGINE_PTR pEngine, SDL_Renderer* pRenderer);


#ifdef __cplusplus
}
#endif

#endif