#include "engine.h"

#include "texture_private.h"


SDL_Texture* 
engine_create_texture_from_bmp(IN WINMO_ENGINE_PTR pEngine, const char* pBmpFile)
{
    SDL_Surface* loadedImage = WINMO_NULL;
    SDL_Texture* texture = WINMO_NULL;
    const char* err_message = WINMO_NULL;

    if(!pEngine || !pBmpFile) {
        fprintf(stderr, "%s: incorrect arguments \n", __FUNCTION__);
        return WINMO_NULL;
    }

    loadedImage = SDL_LoadBMP(pBmpFile);

    if (loadedImage != WINMO_NULL) {
        texture = SDL_CreateTextureFromSurface((SDL_Renderer*)pEngine->pRenderer, loadedImage);
        if (texture) {
            printf("Created texture from BMP: %s \n", pBmpFile);
            SDL_FreeSurface(loadedImage);
        }
        else {
            err_message = SDL_GetError();
            fprintf(stderr, "SDL can't create texture from surface %s: %s \n", pBmpFile, err_message);
            return WINMO_NULL;
        }
    }
    else {
        err_message = SDL_GetError();
        fprintf(stderr, "SDL can't load BMP %s: %s \n", pBmpFile, err_message);
        return WINMO_NULL;
    }

    return texture;
}


SDL_Texture* 
engine_create_texture_from_png(IN WINMO_ENGINE_PTR pEngine, const char* pPngFile)
{
    SDL_Surface* loadedImage = WINMO_NULL;
    SDL_Texture* texture = WINMO_NULL;
    const char* err_message = WINMO_NULL;

    if(!pEngine || !pPngFile) {
        fprintf(stderr, "%s: incorrect arguments \n", __FUNCTION__);
        return WINMO_NULL;
    }

    loadedImage = IMG_Load(pPngFile);

    if (loadedImage != WINMO_NULL) {
        texture = SDL_CreateTextureFromSurface((SDL_Renderer*)pEngine->pRenderer, loadedImage);
        if(! texture){
            err_message = SDL_GetError();
            fprintf(stderr, "SDL can't create texture from surface %s: %s \n", pPngFile, err_message);;
            return WINMO_NULL;
        } else {
            SDL_FreeSurface(loadedImage);
            printf("Created texture from PNG: %s \n", pPngFile);
        }
    }
    else {
        err_message = IMG_GetError();
        fprintf(stderr, "Can't create texture from PNG: %s\n", err_message);
        return WINMO_NULL;
    }

    return texture;
}


WINMO_RESULT
engine_texture_destroy(SDL_Texture* pTexture)
{
    if(! pTexture)
        return WINMO_E_INVARGS;

    SDL_DestroyTexture(pTexture);

    return WINMO_OK;
}


WINMO_RESULT
engine_get_texture_size(SDL_Texture* pTexture, WINMO_INT* pWidth, WINMO_INT* pHeight)
{
    WINMO_INT w = 0;
    WINMO_INT h = 0;

    SDL_QueryTexture(pTexture, NULL, NULL, &w, &h);

    if( w || h ) {
        *pWidth = w;
        *pHeight = h; 
        return WINMO_OK;
    }
    else
        return WINMO_E_FONT_GET_TEX_SIZE;
}
