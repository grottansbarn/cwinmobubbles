#ifndef TEXTURE_PRIVATE_H
#define TEXTURE_PRIVATE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "engine.h"

struct tagWinMoTexture {
    SDL_Texture* pTexture;
    WINMO_PCHAR pPathToFile;
};  

#ifdef __cplusplus
}
#endif

#endif // TEXTURE_PRIVATE_H