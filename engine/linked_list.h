#ifndef linked_list_h
#define linked_list_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>

#include "macro.h"
#include "types.h"

typedef WINMO_INT tData;

typedef struct tagLinkListNode{
    tData value;
    struct tagLinkListNode *next;
} LinkListNode, *LINK_LIST_NODE_PTR;

LINK_LIST_NODE_PTR create_list(WINMO_INT number);

WINMO_VOID print_list(LINK_LIST_NODE_PTR pList);

WINMO_VOID delete_list(LINK_LIST_NODE_PTR pList);

#ifdef __cplusplus
}
#endif

#endif /* linked_list_h */