#include <stdio.h>

#include "engine.h"


#define DEBUG_ENGINE
#ifdef DEBUG_ENGINE
  #define DEBUG_ENGINE_MESSAGE printf
#else
  #define DEBUG_ENGINE_MESSAGE
#endif

#define WINMO_AUDIO_DRIVERS_MAX 16


WINMO_PCHAR
engine_get_sound_format_name(uint16_t format_value)
{
    switch (format_value)
    {
    case AUDIO_U8:
        return "AUDIO_U8";
    case AUDIO_S8:
        return "AUDIO_S8";
    case AUDIO_U16LSB:
        return "AUDIO_U16LSB";
    case AUDIO_S16LSB:
        return "AUDIO_S16LSB";
    case AUDIO_U16MSB:
        return "AUDIO_U16MSB";
    case AUDIO_S16MSB:
        return "AUDIO_S16MSB";
    case AUDIO_S32LSB:
        return "AUDIO_S32LSB";
    case AUDIO_S32MSB:
        return "AUDIO_S32MSB";
    case AUDIO_F32LSB:
        return "AUDIO_F32LSB";
    case AUDIO_F32MSB:
        return "AUDIO_F32MSB";
    default:
        return "Sound format not found";
    }
}


WINMO_INT
engine_get_sound_format_size(uint16_t format_value)
{
    switch (format_value)
    {
    case AUDIO_U8:
        return 1;
    case AUDIO_S8:
        return 1;
    case AUDIO_U16LSB:
        return 2;
    case AUDIO_S16LSB:
        return 2;
    case AUDIO_U16MSB:
        return 2;
    case AUDIO_S16MSB:
        return 2;
    case AUDIO_S32LSB:
        return 4;
    case AUDIO_S32MSB:
        return 4;
    case AUDIO_F32LSB:
        return 4;
    case AUDIO_F32MSB:
        return 4;
    default:
        DEBUG_ENGINE_MESSAGE("Sound format size not found\n");
        return WINMO_E_SDL;
    }
}


WINMO_RESULT
engine_sound_init(IN OUT WINMO_ENGINE_PTR pEngine)
{
    WINMO_RESULT result = WINMO_OK;
    WINMO_PCHAR selected_audio_driver = WINMO_NULL;
    WINMO_INT audio_drivers_number = 0;
    char* audio_drivers_arr[WINMO_AUDIO_DRIVERS_MAX];
    WINMO_PCHAR pzsErr = WINMO_NULL;

    if(!pEngine)
        return WINMO_E_INVARGS;

#ifdef __unix__
    putenv((char *)"SDL_AUDIODRIVER=alsa");
#elif _WIN32
    putenv((char *)"SDL_AUDIODRIVER=DirectSound");
#endif

    audio_drivers_number = SDL_GetNumAudioDrivers();

    // initialize audio
    pEngine->audio_device_spec.freq = 44100;  //44100 48000

    // { AUDIO_U8, 1 }, { AUDIO_S8, 1 }, { AUDIO_U16LSB, 2 },
    // { AUDIO_S16LSB, 2 }, { AUDIO_U16MSB, 2 }, { AUDIO_S16MSB, 2 },
    // { AUDIO_S32LSB, 4 }, { AUDIO_S32MSB, 4 }, { AUDIO_F32LSB, 4 },
    // { AUDIO_F32MSB, 4 },

    pEngine->audio_device_spec.format = AUDIO_S16LSB; //AUDIO_S16LSB;
#ifdef __ANDROID__
    pEngine->audio_device_spec.channels = 2;
#else
    pEngine->audio_device_spec.channels = 2;
#endif
    pEngine->audio_device_spec.samples = 4096; // must be power of 2
    pEngine->audio_device_spec.callback = WINMO_NULL; // soundmanager_audio_callback;
    pEngine->audio_device_spec.userdata = pEngine;

    // printing all and selecting audio DRIVER
    DEBUG_ENGINE_MESSAGE("Found (%d) audio drivers:\n", audio_drivers_number);
    for (WINMO_INT i = 0; i < audio_drivers_number; ++i)
    {
        WINMO_PCHAR tmp_audio_driver_name = SDL_GetAudioDriver(i);
        unsigned long tmp_audio_driver_name_size = strlen(tmp_audio_driver_name);

        audio_drivers_arr[i] = malloc(sizeof(tmp_audio_driver_name[0]) * (tmp_audio_driver_name_size + 1));
        strncpy(audio_drivers_arr[i], tmp_audio_driver_name, (tmp_audio_driver_name_size + 1));

        DEBUG_ENGINE_MESSAGE("\t%d) audio driver: %s \n", i, audio_drivers_arr[i]);
    }

    // TODO on windows 10 only DirectSound - works for me
    const char* OS = SDL_GetPlatform();
    DEBUG_ENGINE_MESSAGE("Audio platform: %s \n", OS);

    for (WINMO_INT i = 0; i < audio_drivers_number; ++i)
    {
        if (0 != SDL_AudioInit(audio_drivers_arr[i])) {
            pzsErr = SDL_GetError();
            DEBUG_ENGINE_MESSAGE("\t(!) can't init SDL audio system with driver: %s (%s)\n", audio_drivers_arr[i], pzsErr);
            continue;
        }
        else {
            DEBUG_ENGINE_MESSAGE("- init SDL audio system with driver: %s \n", audio_drivers_arr[i]);
        }

        // due to unknown reason selected_audio_device become "" after exit from loop 
        selected_audio_driver = audio_drivers_arr[i];

        if (0 == strncmp(OS, "Windows", 5) && 0 == strncmp(audio_drivers_arr[i], "winmm", 5)) {
            break;
        }

        if ((0 == strncmp(OS, "Linux", 5) || 0 == strncmp(OS, "Android", 7)) && 0 == strncmp(selected_audio_driver, "pulseaudio", 10)) {
            break;
        }
        //else {
            // any other OS
            // selected_audio_driver = SDL_GetAudioDriver(1);
            //break;
        //}
    }

    DEBUG_ENGINE_MESSAGE("Selected driver: '%s' for %s \n", selected_audio_driver, OS);

    ///////////////////////////////////////////////////////////////
    // printing all and selecting audio DEVICE
    const char* default_audio_device_name = WINMO_NULL;

    const int audio_devices_number = SDL_GetNumAudioDevices(SDL_FALSE);

    if (audio_devices_number > 0) {
        DEBUG_ENGINE_MESSAGE("Found (%d) audio devices:\n", audio_devices_number);
        for (int i = 0; i < audio_devices_number; ++i) {
            DEBUG_ENGINE_MESSAGE("\t%d) %s\n", i, SDL_GetAudioDeviceName(i, SDL_FALSE));
        }
    }

#ifdef _WIN32   // on Windows better start from the begining of the list
    for (int i = 0; i < audio_devices_number - 1; ++i) {
#else           // on UNIX better start from the end of the list
    for (int i = 0; i <= audio_devices_number - 1; ++i) {
    // for (int i = audio_devices_number - 1; i >= 0; --i) {
#endif
        // set the audio device number audio_devices_number..0
        default_audio_device_name = SDL_GetAudioDeviceName(i, SDL_FALSE);
        pEngine->audio_device = SDL_OpenAudioDevice(default_audio_device_name, 0, &(pEngine->audio_device_spec), WINMO_NULL, SDL_AUDIO_ALLOW_ANY_CHANGE);

        if (0 == pEngine->audio_device || pEngine->audio_device < 2) {
            DEBUG_ENGINE_MESSAGE("-- failed open audio device: %s", SDL_GetError());
            continue;
        }
        else {
            DEBUG_ENGINE_MESSAGE("\nSelected audio device: '%s'\n", default_audio_device_name);
            DEBUG_ENGINE_MESSAGE("\tSDL_AudioDeviceID: %d \n", pEngine->audio_device);
            DEBUG_ENGINE_MESSAGE("\tfreq: %d \n", pEngine->audio_device_spec.freq);
            DEBUG_ENGINE_MESSAGE("\tformat: %s \n", engine_get_sound_format_name(pEngine->audio_device_spec.format));
            DEBUG_ENGINE_MESSAGE("\tchannels: %d \n", (uint32_t)pEngine->audio_device_spec.channels);
            DEBUG_ENGINE_MESSAGE("\tsamples: %d \n", pEngine->audio_device_spec.samples);

            // unpause device
            SDL_PauseAudioDevice((SDL_AudioDeviceID)pEngine->audio_device, SDL_FALSE);
            break;
        }
    }

    return result;
}


WINMO_RESULT
engine_create(IN OUT WINMO_ENGINE_PTR* pEngine)
{
    WINMO_ENGINE_PTR pEtemp = WINMO_NULL;
    SDL_DisplayMode DM;
    const char* err_message = WINMO_NULL;
    WINMO_INT w = 0;
    WINMO_INT h = 0;
    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };
    WINMO_INT linked_version = 0;
    WINMO_RESULT result = 0;

    if(*pEngine)
        return WINMO_E_INVARGS;

    pEtemp = (WINMO_ENGINE_PTR)malloc(sizeof(WINMO_Engine));
    if(!pEtemp)
        return WIMNO_E_OUTOFMEMORY;

    SDL_VERSION(&compiled);
    SDL_GetVersion(&linked);
    DEBUG_ENGINE_MESSAGE("Compiled with SDL2: %d.%d.%d\n", compiled.major, compiled.minor, compiled.patch);
    linked_version = SDL_VERSIONNUM(linked.major, linked.minor, linked.patch);
    if (linked.major < 2) {
        fprintf(stderr, "Error: SDL2 2.+ required: %d\n", linked_version);
        result = WINMO_ERROR;
        goto error_exit;
    }
    else {
        DEBUG_ENGINE_MESSAGE("Linked with SDL2:   %d.%d.%d\n", linked.major, linked.minor, linked.patch);
    }

    //SDL_setenv("SDL_AUDIODRIVER", "pulse", 0); //alsa

    result = SDL_Init(SDL_INIT_EVERYTHING);
    if (result != 0) {
        err_message = SDL_GetError();
        fprintf(stderr,"Can't initialize SDL2: %s\n", err_message);
        result = WINMO_E_SDL;
        goto error_exit;
    }

    int flags = IMG_INIT_PNG;
    if ( !( IMG_Init( flags ) & flags ) ) {
        err_message = IMG_GetError();
        fprintf(stderr,"Can't initialize SDL2_image: %s\n", err_message);
        result = WINMO_E_SDL;
        goto error_exit;
    }

    if (TTF_Init() != 0) {
        err_message = TTF_GetError();
        fprintf(stderr,"Can't initialize SDL2_ttf: %s\n", err_message);
        result = WINMO_E_SDL;
        goto error_exit;
    }

    SDL_GetCurrentDisplayMode(0, &DM);
    DEBUG_ENGINE_MESSAGE("screen refresh rate: %d fps\n", DM.refresh_rate);

    w = DM.w;
    h = DM.h;

    pEtemp->screen_height = h / 2;
    pEtemp->screen_width = w / 2; //1.116

    // opening window
    pEtemp->pWindow = SDL_CreateWindow("WinMo Bubbles",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, pEtemp->screen_width, pEtemp->screen_height,
        SDL_WINDOW_SHOWN);
    if (pEtemp->pWindow == WINMO_NULL) {
        err_message = SDL_GetError();
        fprintf(stderr, "Can't create window: %s\n", err_message);
        result = WINMO_ERROR;
        goto error_exit_1;
    }

    pEtemp->pRenderer = SDL_CreateRenderer(pEtemp->pWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (pEtemp->pRenderer == WINMO_NULL) {
        err_message = SDL_GetError();
        fprintf(stderr, "Can't create renderer: %s\n", err_message);
        result = WINMO_ERROR;
        goto error_exit_2;
    }

    SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);
    SDL_EventState(SDL_WINDOWEVENT, SDL_IGNORE);

    pEtemp->loop = WINMO_TRUE;

    *pEngine = (WINMO_ENGINE_PTR)pEtemp;

    return WINMO_OK;

error_exit_2:
    SDL_DestroyWindow(pEtemp->pWindow);

error_exit_1:
    WINMO_FREE(pEtemp);

error_exit:
    SDL_Quit();
    IMG_Quit();

    return result;
}


WINMO_RESULT
engine_update_event(IN WINMO_ENGINE_PTR pEngine)
{
    // while (WINMO_TRUE) {
        // collect all events from SDL
        SDL_Event sdl_event;
        if (SDL_WaitEvent(&sdl_event)) { // SDL_PollEvent or SDL_WaitEvent
            // mouse
            if (sdl_event.type == SDL_MOUSEBUTTONDOWN) {
                DEBUG_ENGINE_MESSAGE("Mouse button pressed\n");
                pEngine->event = event_mouse;
                pEngine->event_type = eventtype_pressed;
                pEngine->mouse_coord_pressed.x = sdl_event.button.x;
                pEngine->mouse_coord_pressed.y = sdl_event.button.y;
                return WINMO_OK;
            }

            if (sdl_event.type == SDL_MOUSEBUTTONUP) {
                DEBUG_ENGINE_MESSAGE("Mouse button released\n");
                pEngine->event = event_mouse;
                pEngine->event_type = eventtype_released;
                pEngine->mouse_coord_released.x = sdl_event.button.x;
                pEngine->mouse_coord_released.y = sdl_event.button.y;
                return WINMO_OK;
            }

            // buttons
            if (sdl_event.type == SDL_QUIT) {
                pEngine->event = event_turn_off;
                pEngine->event_type = eventtype_non;
                DEBUG_ENGINE_MESSAGE("EXIT \n");
                return WINMO_OK;
            }
            else if (sdl_event.type == SDL_KEYDOWN) {
                DEBUG_ENGINE_MESSAGE("pressed  ");
                pEngine->event_type = eventtype_pressed;
            }
            else if (sdl_event.type == SDL_KEYUP) {
                DEBUG_ENGINE_MESSAGE("released ");
                pEngine->event_type = eventtype_released;
            }

            switch (sdl_event.key.keysym.sym)
            {
                case SDLK_RIGHT:
                    pEngine->event = event_right;
                    DEBUG_ENGINE_MESSAGE("RIGHT button \n");
                    break;
                case SDLK_LEFT:
                    pEngine->event = event_left;
                    DEBUG_ENGINE_MESSAGE("LEFT button \n");
                    break;
                case SDLK_DOWN:
                    pEngine->event = event_down;
                    DEBUG_ENGINE_MESSAGE("DOWN button \n");
                    break;
                case SDLK_UP:
                    pEngine->event = event_up;
                    DEBUG_ENGINE_MESSAGE("UP button \n");
                    break;
                case SDLK_RETURN:
                    pEngine->event = event_enter;
                    DEBUG_ENGINE_MESSAGE("ENTER button \n");
                    break;
                case SDLK_ESCAPE:
                    pEngine->event = event_escape;
                    DEBUG_ENGINE_MESSAGE("ESCAPE button \n");
                    break;
                default:{
                    pEngine->event = event_non;
                    // DEBUG_ENGINE_MESSAGE("unhandled button sdl_event.key.keysym.sym=%4X \n", sdl_event.key.keysym.sym);
                }
            }
        }
        return WINMO_OK;
    // }

    // return WINMO_OK;
}


WINMO_RESULT
engine_destroy(IN WINMO_ENGINE_PTR pEngine)
{
    if(!pEngine)
        return WINMO_OK;

    SDL_DestroyWindow(pEngine->pWindow);
    SDL_DestroyRenderer(pEngine->pRenderer);
    SDL_Quit();
    IMG_Quit();

    WINMO_FREE(pEngine);

    pEngine = WINMO_NULL;

    return WINMO_OK;
}
