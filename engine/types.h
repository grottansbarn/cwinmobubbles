#ifndef TYPES_H
#define TYPES_H

#ifdef __cplusplus
extern "C" {
#endif

#define WINMO_TRUE                  1
#define WINMO_FALSE                 0

#define WINMO_RESULT                int

#define WINMO_VOID                  void
#define WINMO_PVOID                 void*
#define WINMO_NULL                  NULL
#define WINMO_NULLPTR               nullptr
#define WINMO_BOOL                  uint8_t
#define WINMO_INT                   int
#define WINMO_PCHAR                 const char*
#define WINMO_SIZE                  size_t

#ifdef __cplusplus
}
#endif

#endif