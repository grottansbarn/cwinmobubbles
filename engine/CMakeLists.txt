cmake_minimum_required(VERSION 3.16)

project(engine)

file(GLOB ENGINE_SRC_FILES
    "${CMAKE_CURRENT_LIST_DIR}/*.c"
)

file(GLOB ENGINE_HDR_FILES
    "${CMAKE_CURRENT_LIST_DIR}/*.h"
)
list(FILTER ENGINE_HDR_FILES EXCLUDE REGEX "_private")
# message(STATUS "engine headers:\n ${ENGINE_HDR_FILES}")

add_library(${PROJECT_NAME} STATIC ${ENGINE_HDR_FILES} ${ENGINE_SRC_FILES})


if(WIN32)
    target_compile_definitions(${PROJECT_NAME} PRIVATE "-DWINMO_DECLSPEC=__declspec(dllexport)")
else()
    add_definitions(-DC99 -Wall)
    add_definitions(-fno-strict-aliasing -fno-stack-protector -fPIC)
    add_definitions(-D__LINUX__ -D__linux__ -D__POSIX__ -D_GNU_SOURCE)

endif()

set_property(TARGET ${PROJECT_NAME} PROPERTY OUTPUT_NAME ${PROJECT_NAME})
set_property(TARGET ${PROJECT_NAME} PROPERTY C_STANDARD 99)

install(FILES ${ENGINE_HDR_FILES} DESTINATION "include")
set_property(TARGET ${PROJECT_NAME} PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_LIST_DIR}/include")

target_link_libraries(${PROJECT_NAME} ${CONAN_LIBS}) # SDL2 SDL2main gtest gmock) # SDL2_image SDL2_ttf)



# add more warnings
SET(WARNING_FLAGS_COMMON "-Wno-address \
-Wno-unused-variable \
-Wno-unused-function \
-Wno-unused-local-typedefs \
-Wno-missing-braces \
-Wno-parentheses \
-Wno-logical-not-parentheses \
-Wno-format-overflow \
-Wno-format-truncation \
-Wno-sign-compare \
-Wno-misleading-indentation")
SET(WARNING_FLAGS_C "-Wno-overflow \
-Wno-int-conversion \
-Wno-incompatible-pointer-types \
-Wno-incompatible-pointer-types \
-Wdiscarded-qualifiers \
-Wno-implicit-function-declaration \
-Wno-discarded-qualifiers \
-Wno-pointer-to-int-cast \
-Wno-pointer-sign")
SET(WARNING_FLAGS_CXX "-Wno-invalid-offsetof \
-Wno-unknown-pragmas \
-Wno-unused-but-set-variable \
-Wno-unused-value \
-Wno-maybe-uninitialized \
-Wno-narrowing")

macro(OS_compiler_common lang)
    SET(CMAKE_${lang}_FLAGS "${WARNING_FLAGS_COMMON}")
    SET(CMAKE_${lang}_FLAGS_RELEASE "-O2 -DNDEBUG")
    SET(CMAKE_${lang}_FLAGS_RELWITHDEBINFO "-O2 -DNDEBUG -g1")
    SET(CMAKE_${lang}_FLAGS_DEBUG "-ggdb3 -O0 -D_DEBUG")
endmacro()

OS_compiler_common(C)
OS_compiler_common(CXX)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${WARNING_FLAGS_C}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${WARNING_FLAGS_CXX}")

set(CMAKE_EXE_LINKER_FLAGS_INIT "-L${TOOLCHAIN_MISSING_LIB64_PATH} -ldl -lpthread")
set(CMAKE_SHARED_LINKER_FLAGS_INIT "-L${TOOLCHAIN_MISSING_LIB64_PATH} -ldl -lpthread")