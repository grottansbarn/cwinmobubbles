#include "engine.h"

#define DEBUG_SOUND
#ifdef DEBUG_SOUND
  #define DEBUG_SOUND_MESSAGE printf
#else
  #define DEBUG_SOUND_MESSAGE
#endif


WINMO_RESULT
engine_soundbuffer_create(IN WINMO_ENGINE_PTR pEngine, IN OUT SOUNDBUFFER_PTR* ppSoundBuf, const char* path)
{
    SOUNDBUFFER_PTR pSB = WINMO_NULL;
    WINMO_RESULT result = WINMO_OK;
    uint8_t* buffer;
    uint32_t length = 0;

    if(!pEngine || !ppSoundBuf || *ppSoundBuf || !path)
        return WINMO_E_INVARGS;

    pSB = WINMO_MALLOC(SoundBuffer, 1);
    if(!pSB)
        return WIMNO_E_OUTOFMEMORY;

    SDL_LockAudioDevice(pEngine->audio_device);

    // sound_buffer_impl* s = new sound_buffer_impl(path, audio_device, audio_device_spec);
    SDL_RWops* file = SDL_RWFromFile(path, "rb");
    if (WINMO_NULL == file) {
        free(pSB);
        fprintf(stderr, "Can't open sound file '%s' with error: %s\n", path, SDL_GetError());
        return WINMO_E_FILE;
    }

    // freq, format, channels, and samples - used by SDL_LoadWAV_RW
    SDL_AudioSpec file_audio_spec;
    // maybe unused ???
    buffer = calloc(1024*1024, sizeof *buffer); // FIXME: leg
    if (WINMO_NULL == buffer) {
        SDL_RWclose(file);
        free(pSB);
        fprintf(stderr,"Can't open sound file '%s' with error: %s\n", path, SDL_GetError());
        return WINMO_E_FILE;
    }

    if (WINMO_NULL == SDL_LoadWAV_RW(file, 1, &file_audio_spec, &buffer, &length)) {
        free(pSB);
        fprintf(stderr,"Can't load WAV file '%s' with error: %s\n", path, SDL_GetError());
        return WINMO_E_FILE;
    }

    DEBUG_SOUND_MESSAGE("Loaded audio file '%s':\n", path);
    DEBUG_SOUND_MESSAGE("\tformat: %s \n", engine_get_sound_format_name(file_audio_spec.format));
    DEBUG_SOUND_MESSAGE("\tsample_size: %d \n", engine_get_sound_format_size(file_audio_spec.format));
    DEBUG_SOUND_MESSAGE("\tchannels: %d \n", (uint32_t)(file_audio_spec.channels));
    DEBUG_SOUND_MESSAGE("\tfrequency: %d \n", file_audio_spec.freq);
    DEBUG_SOUND_MESSAGE("\tlength: %d \n", length);
    DEBUG_SOUND_MESSAGE("\ttime: %f sec \n", (double)length / (file_audio_spec.channels * file_audio_spec.freq * engine_get_sound_format_size(file_audio_spec.format)));

    if (file_audio_spec.channels != pEngine->audio_device_spec.channels || file_audio_spec.format != pEngine->audio_device_spec.format || file_audio_spec.freq != pEngine->audio_device_spec.freq) {
        SDL_AudioCVT cvt;
        result = SDL_BuildAudioCVT(&cvt, file_audio_spec.format,
            file_audio_spec.channels, file_audio_spec.freq,
            pEngine->audio_device_spec.format, pEngine->audio_device_spec.channels,
            pEngine->audio_device_spec.freq);
        if(-1 == result)
            return WINMO_E_SDL;
        SDL_assert(cvt.needed); // obviously, this one is always needed.
        // read your data into cvt.buf here.
        cvt.len = (int)length;
        // we have to make buffer for inplace conversion
        pSB->tmp_buf = malloc((unsigned long)(cvt.len * cvt.len_mult));
        //uint8_t* buf = malloc((unsigned long)(cvt.len * cvt.len_mult));
        memcpy(pSB->tmp_buf, buffer, (unsigned long)(cvt.len * cvt.len_mult));

        cvt.buf = pSB->tmp_buf;
        if (0 != SDL_ConvertAudio(&cvt)) {
            const char* err = SDL_GetError();
            DEBUG_SOUND_MESSAGE("failed to convert audio from file: %s  to audio device format, err: %s \n", path, err);
        }
        // cvt.buf has cvt.len_cvt bytes of converted data now.
        SDL_FreeWAV(buffer);

        buffer = malloc(cvt.len_cvt);
        memcpy(buffer, cvt.buf, cvt.len_cvt);
        length = (uint32_t)(cvt.len_cvt);
    }

    // set some fields
    pSB->is_playing = WINMO_FALSE;
    pSB->is_looped = WINMO_FALSE;
    pSB->buffer = pSB->tmp_buf;
    pSB->length = length;

    SDL_UnlockAudioDevice(pEngine->audio_device);

    WINMO_SIZE tmp = strlen(path);
    pSB->path_to_src_file = (const char*)malloc(tmp + 1);
    strncpy((char*)pSB->path_to_src_file, path, tmp + 1);

    DEBUG_SOUND_MESSAGE("Sound buffer created %p from file %s \n", (void*)pSB, pSB->path_to_src_file);

    *ppSoundBuf = (SOUNDBUFFER_PTR)pSB;

    return result;
}


WINMO_RESULT
engine_soundbuffer_destroy(IN SOUNDBUFFER_PTR pSoundBuf)
{
    DEBUG_SOUND_MESSAGE("SoundBuffer '%s' is destroyed.\n", pSoundBuf->path_to_src_file);

    if(!pSoundBuf)
        return WINMO_E_INVARGS;

    free((char*)pSoundBuf->path_to_src_file);
    free(pSoundBuf->buffer);
    free(pSoundBuf);
    pSoundBuf = WINMO_NULL;

    return WINMO_OK;
}


WINMO_RESULT
engine_soundbuffer_play(IN WINMO_ENGINE_PTR pEngine, IN SOUNDBUFFER_PTR pSoundBuf, IN SoundPlayProp prop)
{
    WINMO_RESULT result = WINMO_OK;
    WINMO_PCHAR err_message = WINMO_NULL;

    DEBUG_SOUND_MESSAGE("Sound: ad (%d), buf (%p), len (%d)\n",
        pEngine->audio_device, pSoundBuf->buffer, pSoundBuf->length);

    if(WINMO_FAILED(result = SDL_QueueAudio(pEngine->audio_device, pSoundBuf->buffer, pSoundBuf->length))) {
        err_message = SDL_GetError();
        fprintf(stderr, "SDL can't create texture from surface (%X): %s \n", result, err_message);
        return WINMO_E_SOUND_QUEUE;
    }

    // turn off pause on device
    SDL_PauseAudioDevice(pEngine->audio_device, 0);

    DEBUG_SOUND_MESSAGE("Sound '%s' added into mixer\n", pSoundBuf->path_to_src_file);

    return WINMO_OK;
}
