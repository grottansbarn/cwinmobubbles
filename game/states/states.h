#ifndef STATES_H
#define STATES_H

#ifdef __cplusplus
extern "C" {
#endif

#include "engine.h"

#include "state_select_mode.h"
#include "state_game_over.h"

typedef enum tagStatesEnum {
    state_select_mode,
    state_game_over,
} StatesEnum;


typedef struct tagStates {
    StatesEnum CurrState;
    STATE_SELECT_MODE_PTR pStateSelectMode;
    STATE_GAME_OVER_PTR pStateGameOver;
} States, *STATES_PTR;


WINMO_RESULT
states_create(IN OUT STATES_PTR* ppStates, IN WINMO_ENGINE_PTR pEngine);


WINMO_RESULT
states_handle_events(IN STATES_PTR pStates);


WINMO_RESULT 
states_update(IN STATES_PTR pStates);


WINMO_RESULT
states_change_state_to(IN STATES_PTR pStates, IN StatesEnum new_state);


WINMO_RESULT
states_destroy(IN STATES_PTR pStates);


//need to implement all this functions in all states
// WINMO_BOOL state_create(IN WINMO_ENGINE_PTR pE, IN OUT STATE_PTR pS);
// WINMO_BOOL state_destroy(IN STATE_PTR pS);
// WINMO_VOID state_cleanup(IN STATE_PTR pS);

// WINMO_VOID state_pause(IN STATE_PTR pS);
// WINMO_VOID state_resume(IN STATE_PTR pS);

// WINMO_VOID state_handle_events(IN STATE_PTR pS);
// WINMO_VOID state_update(IN STATE_PTR pS);
// WINMO_VOID state_draw(IN STATE_PTR pS);
// WINMO_VOID state_destroy(IN STATE_PTR pS)

#ifdef __cplusplus
}
#endif

#endif
