#include <stdio.h>

#include "state_select_mode.h"

#include "states.h"

extern STATES_PTR pStates;
extern WINMO_ENGINE_PTR pEngine;

WINMO_RESULT
state_select_mode_create(IN OUT STATE_SELECT_MODE_PTR* ppState, IN WINMO_ENGINE_PTR pEngine)
{
    STATE_SELECT_MODE_PTR pS = WINMO_NULL;
    BUTTONS_PTR pBs = WINMO_NULL;
    SOUNDBUFFER_PTR pClick = WINMO_NULL;
    WINMO_RESULT result = WINMO_OK;

    if(*ppState)
        return WINMO_E_INVARGS;

    pS = WINMO_MALLOC(StateSelectMode, 1);
    if(!pS)
        return WIMNO_E_OUTOFMEMORY;

    pS->tex_back = engine_create_texture_from_png(pEngine, "./data/images/gui/back_2.png");
    if (!pS->tex_back){
        result = WINMO_ERROR;
        goto error_exit_tex_error;
    }

    if(WINMO_FAILED(result = buttons_create(pEngine, &pBs)))
        goto error_exit_buttons_create;

    if(WINMO_FAILED(result = buttons_add_button(pEngine, pBs, "classic", 32, 100, 100)))
        goto error_exit_button_add;
    if(WINMO_FAILED(result = buttons_add_button(pEngine, pBs, "extreme", 32, 300, 100)))
        goto error_exit_button_add;

    pS->pButtons = pBs;

    if(WINMO_FAILED(result = engine_soundbuffer_create(pEngine, &pClick, "./data/sounds/10_sound_on.wav")))
        goto error_exit_button_add;

    pS->pClickSoundBuf = pClick;

    *ppState = (STATE_SELECT_MODE_PTR)pS;

    return result;

error_exit_button_add:
    buttons_destroy(pS->pButtons);

error_exit_buttons_create:
    engine_texture_destroy(pS->tex_back);

error_exit_tex_error:
    free(pS);

    return result;
}

WINMO_RESULT
state_select_mode_handle_events(IN STATE_SELECT_MODE_PTR pState)
{
    WINMO_RESULT result = WINMO_OK;

    if(!pState || !pEngine)
        return WINMO_E_INVARGS;

    if (pEngine->event == event_turn_off){
        pEngine->loop = WINMO_FALSE;
    }

    if(pEngine->event == event_mouse && pEngine->event_type == eventtype_pressed)
    {
        buttons_get_pressed_index(pEngine, pState->pButtons, pEngine->mouse_coord_pressed.x, pEngine->mouse_coord_pressed.y);
        if(pState->pButtons->button_pressed_index >= 0)
        {
            if(WINMO_FAILED(result = engine_soundbuffer_play(pEngine, pState->pClickSoundBuf, sound_play_once))) {
                printf("Can't play sound: %s \n", pState->pClickSoundBuf->path_to_src_file);
                return result;
            }

            printf("Mouse pressed in borders of button %d '%s'\n", pState->pButtons->button_pressed_index, 
                                                      pState->pButtons->buttons_array[pState->pButtons->button_pressed_index]->text);
        }
    }

    if (pEngine->event == event_escape && pEngine->event_type == eventtype_released){
        if(WINMO_FAILED(result = states_change_state_to(pStates, state_game_over)))
            return result;
    }

    return WINMO_OK;
}

WINMO_RESULT
state_select_mode_update(IN STATE_SELECT_MODE_PTR pState)
{
    WINMO_RESULT result = WINMO_OK;

    if(!pState || !pEngine)
        return WINMO_E_INVARGS;

    if(WINMO_FAILED(result = engine_render_clear(pEngine, pEngine->pRenderer)))
        return result;

    if(WINMO_FAILED(result = engine_render_copy(pEngine, pEngine->pRenderer, pState->tex_back, NULL, NULL)))
        return result;

    if(WINMO_FAILED(result = buttons_render(pEngine, pState->pButtons)))
        return result;

    if(WINMO_FAILED(result = engine_render_present(pEngine, pEngine->pRenderer)))
        return result;

    return result;
}

WINMO_RESULT
state_select_mode_destroy(IN STATE_SELECT_MODE_PTR pState)
{
    if(!pState)
        return WINMO_OK;
    
    buttons_destroy(pState->pButtons);

    engine_soundbuffer_destroy(pState->pClickSoundBuf);

    engine_texture_destroy(pState->tex_back);

    WINMO_FREE(pState);

    pState = NULL;

    return WINMO_OK;
}
