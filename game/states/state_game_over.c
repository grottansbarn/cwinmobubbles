#include "state_game_over.h"

#include "states.h"
extern STATES_PTR pStates;
extern WINMO_ENGINE_PTR pEngine;


#define move_offset 50


WINMO_RESULT
state_game_over_create(IN OUT STATE_GAME_OVER_PTR* ppState, IN WINMO_ENGINE_PTR pEngine)
{
    STATE_GAME_OVER_PTR pState = NULL;
    WINMO_RESULT result = WINMO_OK;

    if(*ppState)
        return WINMO_E_INVARGS;

    pState = WINMO_MALLOC(StateGameOver, 1);
    if(!pState)
        return WIMNO_E_OUTOFMEMORY;

    pState->tex_back = engine_create_texture_from_png(pEngine, "./data/images/gui/back_1.png");
    if (!pState->tex_back) {
        result = WINMO_E_TEX_CREATE_FAIL;
        goto exit_error_1;
    }

    //temp
    pState->font = engine_font_open("./data/font/consola.ttf", 64);
    if(!pState->font) {
        result =  WINMO_E_FONT_OPEN;
        goto exit_error_1;
    }

    SDL_Color color = {0, 0, 255, 0};
    SDL_Color bg_color = {100, 100, 100, 122};
    pState->tex_goodbye = engine_create_texture_from_text("good bye!", pState->font, color, bg_color, (SDL_Renderer*)pEngine->pRenderer);
    if(pState->tex_goodbye == WINMO_NULL) {
        result = WINMO_E_TEX_CREATE_FAIL;
        goto exit_error_1;
    }

    pState->rect_goodbye = WINMO_MALLOC(SDL_Rect, 1);
    if(WINMO_NULL == pState->rect_goodbye)
        return WIMNO_E_OUTOFMEMORY;
    pState->rect_goodbye->x = pState->rect_goodbye->y = 100;
    pState->rect_goodbye->h = pState->rect_goodbye->w = 200;

    //temp

    *ppState = (STATE_GAME_OVER_PTR)pState;

    return WINMO_OK;

exit_error_1:
    free(pState);
    return result;
}


WINMO_RESULT
state_game_over_handle_events(IN STATE_GAME_OVER_PTR pState)
{
    WINMO_RESULT result = WINMO_OK;

    if(!pState)
        return WINMO_E_INVARGS;

    if (pEngine->event == event_turn_off) {
        pEngine->loop = WINMO_FALSE;
    }

    if (pEngine->event == event_right && pEngine->event_type == eventtype_released) {
        pState->rect_goodbye->x += move_offset;
    }
    if (pEngine->event == event_left && pEngine->event_type == eventtype_released) {
        pState->rect_goodbye->x -= move_offset;
    }
    if (pEngine->event == event_up && pEngine->event_type == eventtype_released) {
        pState->rect_goodbye->y -= move_offset;
    }
    if (pEngine->event == event_down && pEngine->event_type == eventtype_released) {
        pState->rect_goodbye->y += move_offset;
    }

    if (pEngine->event == event_escape && pEngine->event_type == eventtype_released) {
        if(WINMO_FAILED(result = states_change_state_to(pStates, state_select_mode)))
            return result;
    }

    return WINMO_OK;
}


WINMO_RESULT
state_game_over_update(IN STATE_GAME_OVER_PTR pState)
{
    WINMO_RESULT result = WINMO_OK;

    if(!pState || !pEngine)
        return WINMO_E_INVARGS;

    if(pStates->CurrState != state_game_over)
        return WINMO_ERROR;

    if(WINMO_FAILED(result = engine_render_clear(pEngine, pEngine->pRenderer)))
        return result;

    if(WINMO_FAILED(result = engine_render_copy(pEngine, pEngine->pRenderer, pState->tex_back, NULL, NULL)))
        return result;

    if(WINMO_FAILED(result = engine_render_copy(pEngine, pEngine->pRenderer, pState->tex_goodbye, NULL, pState->rect_goodbye)))
        return result;

    if(WINMO_FAILED(result = engine_render_present(pEngine, pEngine->pRenderer)))
        return result;

    return result;
}


WINMO_RESULT
state_game_over_destroy(IN STATE_GAME_OVER_PTR pState)
{
    if(!pState)
        return WINMO_OK;

    engine_texture_destroy(pState->tex_back);
    engine_texture_destroy(pState->tex_goodbye);

    WINMO_FREE(pState);

    pState = NULL;

    return WINMO_OK;
}
