#include <stdio.h>

#include "states.h"

extern STATES_PTR pStates;
extern WINMO_ENGINE_PTR pEngine;

WINMO_RESULT
states_create(IN OUT STATES_PTR* ppStates, IN WINMO_ENGINE_PTR pEngine)
{
    STATES_PTR pS = WINMO_NULL;
    STATE_SELECT_MODE_PTR pSSM = WINMO_NULL;
    STATE_GAME_OVER_PTR pSGO = WINMO_NULL;
    WINMO_RESULT result = WINMO_OK;

    if(*ppStates)
        return WINMO_E_INVARGS;

    pS = WINMO_MALLOC(States, 1);
    if(!pS)
        return WIMNO_E_OUTOFMEMORY;

    //create all states
    if(WINMO_FAILED(result = state_select_mode_create(&pSSM, pEngine))){
        fprintf(stderr, "Can't initialize select mode state: 0x%X \n", result);
        goto error_exit_1;
    }
    else {
        pS->pStateSelectMode = pSSM;
    }
    if(WINMO_FAILED(result = state_game_over_create(&pSGO, pEngine))){
        fprintf(stderr, "Can't initialize game over state: 0x%X \n", result);
        goto error_exit_2;
    }
    else {
        pS->pStateGameOver = pSGO;
    }

    pS->CurrState = state_select_mode; // !!! DEBUG state_select_mode  state_game_over

    *ppStates = (STATES_PTR)pS;

    return WINMO_OK;

error_exit_2:
    WINMO_FREE(pSSM);

error_exit_1:
    WINMO_FREE(pS);
    
    return result;
}


WINMO_RESULT
states_handle_events(IN STATES_PTR pStates)
{
    if(!pStates)
        return WINMO_E_INVARGS;

    WINMO_RESULT result = WINMO_OK;

    if(WINMO_FAILED(result = engine_update_event(pEngine)))
        return result;

    switch (pStates->CurrState)
    {
        case state_select_mode:
            if(WINMO_FAILED(result = state_select_mode_handle_events(pStates->pStateSelectMode)))
                return result;
            break;
        case state_game_over:
            if(WINMO_FAILED(result = state_game_over_handle_events(pStates->pStateGameOver)))
                return result;
            break;
    }

    return result;
}


WINMO_RESULT
states_update(IN STATES_PTR pStates)
{
    WINMO_RESULT result = WINMO_OK;

    if(!pStates)
        return WINMO_E_INVARGS;

    switch (pStates->CurrState)
    { 
        case state_select_mode:
            if(WINMO_FAILED(result = state_select_mode_update(pStates->pStateSelectMode)))
                return result;
            break;
        case state_game_over:
            if(WINMO_FAILED(result = state_game_over_update(pStates->pStateGameOver)))
                return result;
            break;
    }

    return result;
}


WINMO_RESULT
states_change_state_to(IN STATES_PTR pStates, IN StatesEnum new_state)
{
    WINMO_RESULT result = WINMO_OK;

    if(!pStates)
        return WINMO_E_INVARGS;

    pStates->CurrState = new_state;

    switch (pStates->CurrState)
    {
        case state_select_mode:
            if(WINMO_FAILED(result = state_select_mode_update(pStates->pStateSelectMode)))
                return result;
            break;
        case state_game_over:
            if(WINMO_FAILED(result = state_game_over_update(pStates->pStateGameOver)))
                return result;
            break;
    }

    return result;
}


WINMO_RESULT
states_destroy(IN STATES_PTR pStates)
{
    if(!pStates)
        return WINMO_OK;

    state_select_mode_destroy(pStates->pStateSelectMode);
    state_game_over_destroy(pStates->pStateGameOver);

    WINMO_FREE(pStates);

    pStates = NULL;

    return WINMO_OK;
}
