#ifndef STATE_GAME_OVER_H
#define STATE_GAME_OVER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "engine.h"


typedef struct tagStateGameOver {
    TTF_Font* font;

    SDL_Texture* tex_back;
    SDL_Texture* tex_goodbye;
    SDL_Rect*    rect_goodbye;
} StateGameOver, *STATE_GAME_OVER_PTR;

WINMO_RESULT
state_game_over_create(IN OUT STATE_GAME_OVER_PTR* ppState, IN WINMO_ENGINE_PTR pEngine);

WINMO_RESULT
state_game_over_handle_events(IN STATE_GAME_OVER_PTR pState);

WINMO_RESULT
state_game_over_update(IN STATE_GAME_OVER_PTR pState);

WINMO_RESULT
state_game_over_destroy(IN STATE_GAME_OVER_PTR pState);

#ifdef __cplusplus
}
#endif

#endif
