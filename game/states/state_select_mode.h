#ifndef STATE_SELECT_MODE_H
#define STATE_SELECT_MODE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "engine.h"

#include "../components/button.h"

typedef struct tagStateSelectMode {
    SDL_Texture* tex_back;
    BUTTONS_PTR  pButtons;
    SOUNDBUFFER_PTR pClickSoundBuf;
} StateSelectMode, *STATE_SELECT_MODE_PTR;

WINMO_RESULT
state_select_mode_create(IN OUT STATE_SELECT_MODE_PTR* ppState, IN WINMO_ENGINE_PTR pEngine);

WINMO_RESULT
state_select_mode_handle_events(IN STATE_SELECT_MODE_PTR pState);

WINMO_RESULT
state_select_mode_update(IN STATE_SELECT_MODE_PTR pState);

WINMO_RESULT
state_select_mode_destroy(IN STATE_SELECT_MODE_PTR pState);

#ifdef __cplusplus
}
#endif

#endif