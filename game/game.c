#include <stdio.h>
#include <stdlib.h>

#include "game.h"

#include "states/states.h"

extern STATES_PTR pStates;
extern WINMO_ENGINE_PTR pEngine;

WINMO_RESULT
game_create(IN OUT GAME_PTR* ppGame)
{
    GAME_PTR pG = WINMO_NULL;
    WINMO_RESULT result = WINMO_OK;

    if(*ppGame)
        return WINMO_E_INVARGS;

    pG = WINMO_MALLOC(Game, 1);
    if(!pG)
        return WIMNO_E_OUTOFMEMORY;

    *ppGame = (GAME_PTR)pG;

    return result;
}


WINMO_RESULT
game_run(IN GAME_PTR pGame)
{
    WINMO_RESULT result = WINMO_OK;

    while(pEngine->loop) {
        if(WINMO_FAILED(result = states_handle_events(pStates))) return result;
        if(WINMO_FAILED(result = states_update(pStates))) return result;
    }

    return result;
}


WINMO_RESULT
game_destroy(IN GAME_PTR pGame)
{
    if(!pGame)
        return WINMO_OK;

    free(pGame);

    pGame = NULL;

    return WINMO_OK;
}


/*****************************************************************************/

WINMO_ENGINE_PTR pEngine = WINMO_NULL;
STATES_PTR pStates  = WINMO_NULL;
GAME_PTR pGame = WINMO_NULL;

int main(/* int argc, char** argv */)
{
    WINMO_RESULT result = WINMO_OK;

    if(WINMO_FAILED(result = engine_create(&pEngine))){ goto error_exit; }

    if(WINMO_FAILED(result = engine_sound_init(pEngine))){ goto error_exit; }

    if(WINMO_FAILED(result = states_create(&pStates, pEngine))){ goto error_exit; }

    if(WINMO_FAILED(result = game_create(&pGame))){ goto error_exit; }

    if(WINMO_FAILED(result = game_run(pGame))){ goto error_exit; }

error_exit:
    states_destroy(pStates);
    engine_destroy(pEngine);
    game_destroy(pGame);

    return result;
}
