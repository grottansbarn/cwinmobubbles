#pragma once

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include "macro.h"

using namespace testing;

TEST(macroh_tests, winmo_malloc_1)
{
    int* size = WINMO_MALLOC(int, 2);

    EXPECT_EQ(sizeof(size), 8);
}

TEST(macroh_tests, winmo_malloc_2)
{
    int* size = WINMO_MALLOC(int, 1);

    EXPECT_EQ(sizeof(size), 8);
}

TEST(macroh_tests, sdl_failed_if_failed)
{
    EXPECT_EQ(true, SDL_FAILED(-1));
}

TEST(macroh_tests, sdl_failed_if_not_failed)
{
    EXPECT_EQ(false, SDL_FAILED(0));
}