#ifndef GAME_H
#define GAME_H

#ifdef __cplusplus
extern "C" {
#endif

#include "engine.h"

#include "states/states.h"


typedef struct tagGame {
    WINMO_INT a;
} Game, *GAME_PTR;


WINMO_RESULT
game_create(IN OUT GAME_PTR* pGame);


WINMO_RESULT
game_run(IN GAME_PTR pGame);


WINMO_RESULT
game_destroy(IN GAME_PTR pGame);

#ifdef __cplusplus
}
#endif

#endif
